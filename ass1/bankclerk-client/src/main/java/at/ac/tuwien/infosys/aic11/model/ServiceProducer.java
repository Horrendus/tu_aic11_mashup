package at.ac.tuwien.infosys.aic11.model;

import at.ac.tuwien.infosys.aic11.client.ServiceClientUtil;
import at.ac.tuwien.infosys.aic11.services.CustomerManagement;
import at.ac.tuwien.infosys.aic11.services.ContractManagement;
import at.ac.tuwien.infosys.aic11.services.CustomerRating;
import at.ac.tuwien.infosys.aic11.services.Shipping;

import javax.enterprise.inject.Produces;
import javax.enterprise.context.RequestScoped;

import static at.ac.tuwien.infosys.aic11.client.ServiceClientUtil.createRatingService;

public class ServiceProducer {

    @Produces @RequestScoped
    public CustomerRating createRating() {
        return createRatingService();
    }

    @Produces @RequestScoped
    public Shipping createShipping() {
        return ServiceClientUtil.createWsServiceClient(Shipping.class);
    }

    @Produces @RequestScoped
    public ContractManagement createContractManagementService() {
        return ServiceClientUtil.createEncryptedWebService(ContractManagement.class);
    }

    @Produces @RequestScoped
    public CustomerManagement createCRMService() {
        return ServiceClientUtil.createWsServiceClient(CustomerManagement.class);
    }
}
