package at.ac.tuwien.infosys.aic11.client;

import at.ac.tuwien.infosys.aic11.main.MainUtils;
import at.ac.tuwien.infosys.aic11.services.CustomerRating;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;

import java.util.HashMap;
import java.util.Map;

public class ServiceClientUtil {
    private static final String WSU_NS = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";

    private ServiceClientUtil() {
    }

    public static <S> S createWsServiceClient(Class<S> serviceType) {
        return createWsServiceClient(MainUtils.getRelServicePath(serviceType), serviceType);
    }

    public static <S> S createWsServiceClient(String relServicePath, Class<S> serviceType) {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.getInInterceptors().add(new LoggingInInterceptor());
        factory.getOutInterceptors().add(new LoggingOutInterceptor());
        factory.setAddress(MainUtils.WS_ADDRESS + relServicePath);
        return factory.create(serviceType);
    }

    public static <S> S createEncryptedWebService(Class<S> serviceType) {

        Map<String, Object> outProps = new HashMap<String, Object>();
        outProps.put("action", "UsernameToken Timestamp Signature Encrypt");

        outProps.put("passwordType", "PasswordDigest");
        outProps.put("user", "clientx509v1");

        outProps.put("passwordCallbackClass", "at.ac.tuwien.infosys.aic11.main.UTPasswordCallback");

        outProps.put("encryptionUser", "serverx509v1");
        outProps.put("encryptionPropFile", "etc/Client_Encrypt.properties");
        outProps.put("encryptionKeyIdentifier", "IssuerSerial");
        outProps.put("encryptionParts", "{Element}{" + WSU_NS + "}Timestamp;"
                + "{Content}{http://schemas.xmlsoap.org/soap/envelope/}Body");

        outProps.put("signaturePropFile", "etc/Client_Sign.properties");
        outProps.put("signatureKeyIdentifier", "DirectReference");
        outProps.put("signatureParts", "{Element}{" + WSU_NS + "}Timestamp;"
                + "{Element}{http://schemas.xmlsoap.org/soap/envelope/}Body");

        Map<String, Object> inProps = new HashMap<String, Object>();

        inProps.put("action", "UsernameToken Timestamp Signature Encrypt");
        inProps.put("passwordType", "PasswordText");
        inProps.put("passwordCallbackClass", "at.ac.tuwien.infosys.aic11.main.UTPasswordCallback");

        inProps.put("decryptionPropFile", "etc/Client_Sign.properties");
        inProps.put("encryptionKeyIdentifier", "IssuerSerial");

        inProps.put("signaturePropFile", "etc/Client_Encrypt.properties");
        inProps.put("signatureKeyIdentifier", "DirectReference");

        S servicePort = createWsServiceClient(serviceType);

        Client client = ClientProxy.getClient(servicePort);
        Endpoint cxfEndpoint = client.getEndpoint();

        WSS4JInInterceptor wssIn = new WSS4JInInterceptor(inProps);
        cxfEndpoint.getInInterceptors().add(wssIn);

        WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
        cxfEndpoint.getOutInterceptors().add(wssOut);

        return servicePort;
    }

    public static CustomerRating createRatingService() {

        return JAXRSClientFactory.create(MainUtils.REST_ADDRESS,CustomerRating.class);
    }
}
