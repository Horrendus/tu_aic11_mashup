package at.ac.tuwien.infosys.aic11.client;

import at.ac.tuwien.infosys.aic11.shipping.generated.ShipContractResponse;
import at.ac.tuwien.infosys.aic11.shipping.generated.Shipping;

import javax.xml.ws.AsyncHandler;
import javax.xml.ws.Response;
import java.io.Serializable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Logger;


public class ShippingClient implements Serializable{


    private static final Logger LOG = Logger.getLogger(ShippingClient.class.getName());

    private Shipping shipping;
    private Future<?> responseCallback;
    private ShipContractCallbackHandler callbackHandler;

    public ShippingClient() {
        shipping = ServiceClientUtil.createWsServiceClient("shipping", Shipping.class);
    }

    public void shipContract(String requestId) {
        this.callbackHandler =  new ShippingClient.ShipContractCallbackHandler();
        responseCallback = shipping.shipContractAsync(requestId, callbackHandler);
        LOG.info("waiting to receive confirmation fax!");

    }

    public boolean getResponse(){
        return this.callbackHandler.getResponse().isReturn();
    }

    public boolean isResponseReceived() {
        return responseCallback.isDone();
    }


    private class ShipContractCallbackHandler implements AsyncHandler<ShipContractResponse> {
        private ShipContractResponse output;

        public void handleResponse(Response<ShipContractResponse> response) {
            try {
                output = response.get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public ShipContractResponse getResponse() {
            return output;
        }
    }
}
