package at.ac.tuwien.infosys.aic11.model;

import at.ac.tuwien.infosys.aic11.client.DisbursementClient;
import at.ac.tuwien.infosys.aic11.main.Data;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.logging.Level;
import java.util.logging.Logger;

@RequestScoped
@Named("disbBean")
public class DisbursementBean {
    private static final Logger LOG = Logger.getLogger(DisbursementBean.class.getName());

    @Inject
    DisbursementClient disbursementClient;

    public String disburse() {
        try {
            disbursementClient.disburse(Data.createCreditRequest());
            return "success";
        } catch (Exception e) {
            LOG.log(Level.WARNING, "disbursement was not been taken", e);
            return "failure";
        }
    }

}
