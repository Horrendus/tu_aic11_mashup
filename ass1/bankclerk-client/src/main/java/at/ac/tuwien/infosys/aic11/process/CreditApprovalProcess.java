package at.ac.tuwien.infosys.aic11.process;


import at.ac.tuwien.infosys.aic11.client.DisbursementClient;
import at.ac.tuwien.infosys.aic11.client.ShippingClient;
import at.ac.tuwien.infosys.aic11.dto.*;
import at.ac.tuwien.infosys.aic11.main.Data;
import at.ac.tuwien.infosys.aic11.services.ContractManagement;
import at.ac.tuwien.infosys.aic11.services.CustomerRating;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class CreditApprovalProcess {
    private static final Logger LOG = Logger.getLogger(CreditApprovalProcess.class.getName());

    @Inject
    CustomerRating customerRating;

    @Inject
    ContractManagement contractManagement;

    @Inject
    DisbursementClient disbursementClient;

    @Inject
    ShippingClient shippingClient;


    public ShippingClient getShippingClient(){
        return shippingClient;
    }



    public String process(CreditRequest creditRequest) {
        assertCreditRequest(creditRequest);
        try {
            if (creditRequest.getState().equals(CreditRequestState.Create)) {
                creditRequest = contractManagement.createContract(creditRequest.getReason(),
                        creditRequest.getCustomer(), creditRequest.getMoney(), creditRequest.getDuration());

                assertCreditRequest(creditRequest);
                assertId(creditRequest);

                creditRequest.setState(CreditRequestState.Offer);
                return process(creditRequest);
            }
            else if (creditRequest.getState().equals(CreditRequestState.Update)) {
                CreditRequest actualCr = contractManagement.updateContract(creditRequest.getRequestId(), creditRequest.getMoney(), creditRequest.getDuration());

                assertCreditRequest(creditRequest);
                assertId(creditRequest);

                creditRequest.setState(CreditRequestState.Offer);
                return process(creditRequest);
            }
            else if (creditRequest.getState().equals(CreditRequestState.Offer)) {
                assertId(creditRequest);
                
                Rating custRating = getRating(creditRequest.getCustomer());
                List<Rating> warrantorsRating = new ArrayList<Rating>();
                if (creditRequest.getWarrantors() != null) {
                    for (Warrantor w : creditRequest.getWarrantors()) {
                        warrantorsRating.add(getRating(w));
                    }
                }
                BigDecimal rating = calculateRating(custRating, warrantorsRating);
                BigDecimal rateForAmount = getRateForAmount(creditRequest.getMoney().getAmount());
                BigDecimal rate = calculateInterestRate(rating, rateForAmount, creditRequest.getDuration());
                rate.setScale(2, BigDecimal.ROUND_HALF_UP);
                InterestRate interestRate = new InterestRate(Double.valueOf(rate.toString()));
                Offer offer = new Offer(java.util.UUID.randomUUID().toString(), "better call saul", interestRate, creditRequest);
                creditRequest.setOffer(offer);
                LOG.info("generated offer:\n" + Data.encodeToXml(creditRequest));
                contractManagement.attachOffer(creditRequest);
                return creditRequest.getRequestId();
            }
            else if (creditRequest.getState().equals(CreditRequestState.Delete)) {
                contractManagement.deleteContract(creditRequest.getRequestId());
            }
            else if (creditRequest.getState().equals(CreditRequestState.Accept)) {
                contractManagement.acceptContract(creditRequest.getRequestId());
                CreditRequest actualCr = contractManagement.getContract(creditRequest.getRequestId());
                assertCreditRequest(creditRequest);
                contractManagement.awaitingContractApproval(actualCr.getRequestId());
                shippingClient.shipContract(actualCr.getRequestId());
                disbursementClient.disburse(actualCr);
                actualCr.setState(CreditRequestState.AwaitingApproval);
            }
            else if (creditRequest.getState().equals(CreditRequestState.AwaitingApproval)) {

                contractManagement.finalizeContract(creditRequest.getRequestId());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "failure";
        }

        return "success";
    }

    private void assertCreditRequest(CreditRequest creditRequest) {
        if (creditRequest == null) {
            throw new IllegalArgumentException("credit request == null");
        }
    }

    private void assertId(CreditRequest creditRequest) {
        if (creditRequest.getRequestId() == null) {
            throw new IllegalStateException("credit request id == null");
        }
    }

    BigDecimal calculateInterestRate(BigDecimal rating, BigDecimal rateForAmount, Duration duration) {
        return rating.multiply(rateForAmount).divide(new BigDecimal(duration.getYears()), 2, BigDecimal.ROUND_HALF_UP).add(new BigDecimal("3.5"));
    }

    BigDecimal getRateForAmount(long amount) {
        if (amount <= 10000) {
            return new BigDecimal("1");
        } else if (amount <= 20000) {
            return new BigDecimal("2");
        } else if (amount <= 50000) {
            return new BigDecimal("3");
        } else {
            return new BigDecimal("5");
        }
    }

    BigDecimal calculateRating(Rating custRating, List<Rating> warrantorsRating) {
        BigDecimal rating = custRating.getCustomerRating().getValue().multiply(new BigDecimal("2"));
        for (Rating r : warrantorsRating) {
            rating = rating.add(r.getCustomerRating().getValue());
        }
        rating = rating.divide(new BigDecimal(warrantorsRating.size() + 2));
        return rating;
    }

    Rating getRating(Customer c) {
        if (c != null && c.getCustomerId() != null) {
            try {
                return customerRating.getRatingForCustomer(c.getCustomerId());
            } catch (Exception e) {
                LOG.info(e.getMessage() + " -> use CustomerRating.A");
            }
        } else {
            LOG.info("customer == null -> use CustomerRating.A");
        }
        return new Rating(c, at.ac.tuwien.infosys.aic11.dto.CustomerRating.A);
    }


}
