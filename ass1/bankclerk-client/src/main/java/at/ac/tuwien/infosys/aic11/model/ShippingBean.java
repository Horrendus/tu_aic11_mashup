package at.ac.tuwien.infosys.aic11.model;


import at.ac.tuwien.infosys.aic11.client.ShippingClient;
import at.ac.tuwien.infosys.aic11.dto.CreditRequest;
import at.ac.tuwien.infosys.aic11.process.CreditApprovalProcess;
import at.ac.tuwien.infosys.aic11.services.ContractManagement;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.logging.Logger;


@Named
@RequestScoped
public class ShippingBean {

   private static final Logger LOG = Logger.getLogger(ShippingBean.class.getName());

    private ShippingClient shippingClient;
    private CreditRequest creditRequest;

    @Inject
    CreditApprovalProcess creditApprovalProcess;

    @Inject
    ContractManagement contractManagement;


    @PostConstruct
    public void setUp(){
        setShippingClient();
        getCreditRequest();
    }


    public boolean isFaxReceived(){
        if(shippingClient.isResponseReceived())
            return shippingClient.getResponse();
        else
            return false;
    }

    public String finalizeContract(){
        String ret = creditApprovalProcess.process(creditRequest);
        if (ret != null && ret.equals("success")) {
            return "home.xhtml?faces-redirect=true";
        }else{
            return "step_finalize.xhtml";
        }
    }


    public void setShippingClient() {
        shippingClient = (ShippingClient)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("shippingClient");
        if (shippingClient == null) {
            throw new IllegalStateException("no shippingClient is set");
        }

    }


    public CreditRequest getCreditRequest() {
        if (creditRequest == null) {

                String requestId = (String)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("requestId");
                if (requestId == null) {
                    throw new IllegalStateException("no credit request id set to update");
                }
                try {
                    creditRequest = contractManagement.getContract(requestId);
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        return creditRequest;
    }


}
