package at.ac.tuwien.infosys.aic11.jsf;

import at.ac.tuwien.infosys.aic11.client.ServiceClientUtil;
import at.ac.tuwien.infosys.aic11.dto.Customer;
import at.ac.tuwien.infosys.aic11.services.CustomerManagement;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

public class CustomerConverter implements Converter {

    private static CustomerManagement customerManagement = ServiceClientUtil.createWsServiceClient(CustomerManagement.class);

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            throw new IllegalArgumentException("value == null");
        }
        String customerId = value.split(" ")[0];
        return customerManagement.getCustomerById(customerId);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return ((Customer) value).toString();
    }
}
