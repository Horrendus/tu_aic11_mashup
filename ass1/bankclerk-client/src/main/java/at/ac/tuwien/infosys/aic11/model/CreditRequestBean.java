package at.ac.tuwien.infosys.aic11.model;

import at.ac.tuwien.infosys.aic11.dto.CreditRequest;
import at.ac.tuwien.infosys.aic11.dto.CreditRequestState;
import at.ac.tuwien.infosys.aic11.dto.Duration;
import at.ac.tuwien.infosys.aic11.dto.Money;
import at.ac.tuwien.infosys.aic11.main.Data;
import at.ac.tuwien.infosys.aic11.process.CreditApprovalProcess;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.logging.Logger;

@RequestScoped
@Named("creditBean")
public class CreditRequestBean {
    private static final Logger LOG = Logger.getLogger(CreditRequestBean.class.getName());
    private CreditRequest creditRequest;

    @Inject
    CreditApprovalProcess creditApprovalProcess;

    private String requestId;

    @PostConstruct
    public void setUp() {
        creditRequest = new CreditRequest();
        creditRequest.setMoney(new Money());
        creditRequest.setDuration(new Duration());
    }

    public void createCreditRequest() {
        LOG.info("create request");
        LOG.info(Data.encodeToXml(creditRequest));
        creditRequest.setState(CreditRequestState.Create);
        requestId = creditApprovalProcess.process(creditRequest);
    }

    public CreditRequest getCreditRequest() {
        return creditRequest;
    }

    public void setCreditRequest(CreditRequest creditRequest) {
        this.creditRequest = creditRequest;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
