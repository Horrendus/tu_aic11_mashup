package at.ac.tuwien.infosys.aic11.client;

import at.ac.tuwien.infosys.aic11.dto.Address;
import at.ac.tuwien.infosys.aic11.dto.CreditRequest;
import at.ac.tuwien.infosys.aic11.dto.Customer;
import at.ac.tuwien.infosys.aic11.dto.Money;
import at.ac.tuwien.infosys.aic11.services.BankTransfer;
import at.ac.tuwien.infosys.aic11.services.Cheque;
import at.ac.tuwien.infosys.aic11.services.DisbursementPreference;
import at.ac.tuwien.infosys.aic11.services.InvalidParameterException;
import at.ac.tuwien.infosys.aic11.services.RegistryService;
import at.ac.tuwien.infosys.aic11.services.IRegistryService;
import at.ac.tuwien.infosys.aic11.services.WsdlEndpoint;
import org.apache.cxf.staxutils.StaxSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import java.io.StringWriter;
import java.net.URL;
import java.util.logging.Logger;

public class DisbursementClient {
    private static final Logger LOG = Logger.getLogger(DisbursementClient.class.getName());
    private static final String DISBURSEMENT_SERVICE_NAME = "Disbursement_Service";
    private static final String PORT_NAME = "Disbursement_Service";

    static JAXBContext jaxbContext = null;

    static {
        try {
            jaxbContext = JAXBContext.newInstance(StartMoneyTransferProcess.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public void disburse(CreditRequest cr) throws Exception {
        DisbursementPreference registryDp = convert(cr.getCustomer().getDisbursementPreference());
        String disbursementLocation = getDisbursementLocation(registryDp);
        delegateDisbursement(disbursementLocation, cr, registryDp);
    }

    String getDisbursementLocation(DisbursementPreference preference) throws InvalidParameterException {
        RegistryService registryService = new RegistryService();
        IRegistryService registry = registryService.getRegistryService();
        WsdlEndpoint wsdlEndpoint = registry.query(preference);
        return wsdlEndpoint.getLocation();
    }

    void delegateDisbursement(String disbursementLocation, CreditRequest cr, DisbursementPreference preference) throws Exception {
        Dispatch<Source> service = getDispatch(disbursementLocation + "?wsdl");
        Source source = getSource(cr, preference);
        LOG.info(sourceToString(source));
        SAXSource retSource = (StaxSource) service.invoke(source);
        LOG.info(sourceToString(retSource));
    }

    String sourceToString(Source source) throws TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        StringWriter writer = new StringWriter();
        StreamResult streamResult = new StreamResult(writer);
        transformer.transform(source, streamResult);
        return writer.toString();
    }

    Dispatch<Source> getDispatch(String wsdlLocation) throws Exception {
        LOG.info("Get service from " + wsdlLocation);
        URL wsdlURL = new URL(wsdlLocation);
        Service service = Service.create(wsdlURL, new QName("http://services.aic11.infosys.tuwien.ac.at/", DISBURSEMENT_SERVICE_NAME));
        return service.createDispatch(new QName("http://services.aic11.infosys.tuwien.ac.at/", PORT_NAME), Source.class, Service.Mode.PAYLOAD);
    }

    Source getSource(CreditRequest cr, DisbursementPreference preference) throws JAXBException {
        Source body = getStartMoneyTransferProcessAsXml(cr, preference);
        return body;
    }

    Source getStartMoneyTransferProcessAsXml(CreditRequest cr, DisbursementPreference preference) throws JAXBException {
        StartMoneyTransferProcess smtp = new StartMoneyTransferProcess(preference, cr.getMoney(), cr.getCustomer());
        return new JAXBSource(jaxbContext, smtp);
    }

    DisbursementPreference convert(at.ac.tuwien.infosys.aic11.dto.DisbursementPreference preference) throws InvalidParameterException {
        if (preference instanceof at.ac.tuwien.infosys.aic11.dto.Cheque) {
            at.ac.tuwien.infosys.aic11.dto.Cheque c = (at.ac.tuwien.infosys.aic11.dto.Cheque) preference;
            Cheque registryCheque = new Cheque();
            registryCheque.setName(c.getName());
            return registryCheque;
        }
        else if (preference instanceof at.ac.tuwien.infosys.aic11.dto.BankTransfer) {
            at.ac.tuwien.infosys.aic11.dto.BankTransfer bt = (at.ac.tuwien.infosys.aic11.dto.BankTransfer) preference;
            BankTransfer registryBankTransfer = new BankTransfer();
            registryBankTransfer.setBankName(bt.getBankName());
            registryBankTransfer.setBic(bt.getBic());
            registryBankTransfer.setIban(bt.getIban());
            return registryBankTransfer;
        }
        throw new InvalidParameterException("Customer has no disbursement preference");
    }

    @XmlRootElement(name = "start_money_transfer_process", namespace = "http://services.aic11.infosys.tuwien.ac.at/")
    @XmlAccessorType(XmlAccessType.PROPERTY)
    @XmlType(propOrder = {
            "arg0",
            "arg1",
            "arg2"
    })
    static class StartMoneyTransferProcess {
        DisbursementPreference arg0;
        MoneyDisbursementAdapter arg1;
        CustomerDisbursementAdapter arg2;

        public StartMoneyTransferProcess(DisbursementPreference arg0, Money arg1, Customer arg2) {
            this.arg0 = arg0;
            this.arg1 = new MoneyDisbursementAdapter(arg1);
            this.arg2 = new CustomerDisbursementAdapter(arg2);
        }

        public StartMoneyTransferProcess() {
        }

        public DisbursementPreference getArg0() {
            return arg0;
        }

        public void setArg0(DisbursementPreference arg0) {
            this.arg0 = arg0;
        }

        public MoneyDisbursementAdapter getArg1() {
            return arg1;
        }

        public void setArg1(MoneyDisbursementAdapter arg1) {
            this.arg1 = arg1;
        }

        public CustomerDisbursementAdapter getArg2() {
            return arg2;
        }

        public void setArg2(CustomerDisbursementAdapter arg2) {
            this.arg2 = arg2;
        }
    }

    @XmlAccessorType(XmlAccessType.PROPERTY)
    @XmlType(propOrder = {
            "amount",
            "currencyCode"
    })
    static class MoneyDisbursementAdapter {
        Money money;

        public MoneyDisbursementAdapter(Money money) {
            this.money = money;
        }

        public MoneyDisbursementAdapter() {
        }

        public String getCurrencyCode() {
            return money.getCurrencyCode();
        }

        public void setCurrencyCode(String currencyCode) {
            money.setCurrencyCode(currencyCode);
        }

        @XmlElement(required = true)
        public long getAmount() {
            return money.getAmount();
        }

        public void setAmount(long amount) {
            money.setAmount(amount);
        }
    }

    @XmlAccessorType(XmlAccessType.PROPERTY)
    @XmlType(propOrder = {
            "address",
            "customerId",
            "firstName",
            "lastName",
            "middleName"
    })
    static class CustomerDisbursementAdapter {
        Customer customer;

        public CustomerDisbursementAdapter(Customer customer) {
            this.customer = customer;
        }

        public  CustomerDisbursementAdapter() {
        }

        @XmlElement(required = true)
        public long getCustomerId() {
            return Long.valueOf(customer.getCustomerId());
        }

        public void setCustomerId(long customerId) {
            customer.setCustomerId(String.valueOf(customerId));
        }

        public String getFirstName() {
            return customer.getFirstName();
        }

        public void setFirstName(String firstName) {
            customer.setFirstName(firstName);
        }

        public String getMiddleName() {
            return customer.getMiddleName();
        }

        public void setMiddleName(String middleName) {
            customer.setMiddleName(middleName);
        }

        public String getLastName() {
            return customer.getLastName();
        }

        public void setLastName(String lastName) {
            customer.setLastName(lastName);
        }

        public AddressDisbursementAdapter getAddress() {
            return new AddressDisbursementAdapter(customer.getAddress());
        }

        public void setAddress(AddressDisbursementAdapter address) {
            customer.setAddress(address.getAddress());
        }
    }

    @XmlAccessorType(XmlAccessType.PROPERTY)
    @XmlType(propOrder = {
            "city",
            "countryCode",
            "door",
            "house",
            "postalCode",
            "street"
    })
    static class AddressDisbursementAdapter {
        Address address;

        public  AddressDisbursementAdapter(Address address) {
            this.address = address;
        }

        public  AddressDisbursementAdapter() {
        }

        @XmlTransient
        public Address getAddress() {
            return address;
        }

        public String getCity() {
            return address.getCity();
        }

        public void setCity(String city) {
            address.setCity(city);
        }

        public String getDoor() {
            return address.getDoor();
        }

        public void setDoor(String door) {
            address.setDoor(door);
        }

        public String getHouse() {
            return address.getHouse();
        }

        public void setHouse(String house) {
            address.setHouse(house);
        }

        public String getStreet() {
            return address.getStreet();
        }

        public void setStreet(String street) {
            address.setStreet(street);
        }

        public String getPostalCode() {
            return address.getPostalCode();
        }

        public void setPostalCode(String postalCode) {
            address.setPostalCode(postalCode);
        }

        public String getCountryCode() {
            return address.getCountryCode();
        }

        public void setCountryCode(String countryCode) {
            address.setCountryCode(countryCode);
        }
    }
}
