package at.ac.tuwien.infosys.aic11.model;

import at.ac.tuwien.infosys.aic11.dto.CreditRequest;
import at.ac.tuwien.infosys.aic11.dto.CreditRequestState;
import at.ac.tuwien.infosys.aic11.dto.Duration;
import at.ac.tuwien.infosys.aic11.dto.Money;
import at.ac.tuwien.infosys.aic11.main.Data;
import at.ac.tuwien.infosys.aic11.process.CreditApprovalProcess;
import at.ac.tuwien.infosys.aic11.services.ContractManagement;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.logging.Logger;

@RequestScoped
@Named("updateBean")
public class UpdateRequestBean {
    private static final Logger LOG = Logger.getLogger(UpdateRequestBean.class.getName());
    private CreditRequest creditRequest;

    @Inject
    CreditApprovalProcess creditApprovalProcess;

    @Inject
    ContractManagement contractManagement;

    private boolean finalized;

    public void updateCreditRequest() {
        LOG.info("updateCreditRequest");
        getCreditRequest().setState(CreditRequestState.Update);
        LOG.info(Data.encodeToXml(creditRequest));
        String requestId = creditApprovalProcess.process(creditRequest);
        creditRequest = contractManagement.getContract(requestId);
    }

    public void refuseCreditRequest() {
        LOG.info("refuseCreditRequest");
        getCreditRequest().setState(CreditRequestState.Delete);
        LOG.info(Data.encodeToXml(creditRequest));
        String ret = creditApprovalProcess.process(creditRequest);
        if (ret != null && ret.equals("success")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Refused credit request"));
            finalized = true;
        }
    }

    public String acceptOffer() {
        LOG.info("acceptOffer");
        LOG.info(Data.encodeToXml(creditRequest));
        getCreditRequest().setState(CreditRequestState.Accept);
        String ret = creditApprovalProcess.process(creditRequest);


        if (ret != null && ret.equals("success")) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("shippingClient",creditApprovalProcess.getShippingClient());
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("requestId",creditRequest.getRequestId());

            return "step_finalize.xhtml";
        }
        return "step_update.xhtml";
    }

    public CreditRequest getCreditRequest() {
        if (creditRequest == null) {
            if (FacesContext.getCurrentInstance().isPostback()) {
                creditRequest = new CreditRequest();
                creditRequest.setMoney(new Money());
                creditRequest.setDuration(new Duration());
            } else {
                String requestId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("requestId");
                if (requestId == null) {
                    throw new IllegalStateException("no credit request id set to update");
                }
                try {
                    creditRequest = contractManagement.getContract(requestId);
                    if(creditRequest.getState() != CreditRequestState.Offer) {
                        throw new IllegalStateException("try to update credit request not in offer state");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return creditRequest;
    }

    public void setCreditRequest(CreditRequest creditRequest) {
        this.creditRequest = creditRequest;
    }

    public boolean isFinalized() {
        return finalized;
    }

    public void setFinalized(boolean finalized) {
        this.finalized = finalized;
    }
}
