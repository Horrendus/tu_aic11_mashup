package at.ac.tuwien.infosys.aic11.model;

import at.ac.tuwien.infosys.aic11.dto.Customer;
import at.ac.tuwien.infosys.aic11.services.CustomerManagement;
import at.ac.tuwien.infosys.aic11.services.CustomerRating;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;

@RequestScoped
@Named("custBean")
public class CustomerBean {
    private Collection<Customer> customers;

    @Inject
    private CustomerManagement customerManagement;

    public Collection<Customer> getAllCustomers() {
        if (customers == null) {
            try {
                customers = customerManagement.getAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return customers;
    }
}
