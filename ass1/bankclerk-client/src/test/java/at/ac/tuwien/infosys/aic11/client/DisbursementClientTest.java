package at.ac.tuwien.infosys.aic11.client;

import java.util.logging.Logger;

import at.ac.tuwien.infosys.aic11.client.DisbursementClient;
import at.ac.tuwien.infosys.aic11.dto.Address;
import at.ac.tuwien.infosys.aic11.dto.CreditRequest;
import at.ac.tuwien.infosys.aic11.main.Data;
import at.ac.tuwien.infosys.aic11.services.Cheque;
import at.ac.tuwien.infosys.aic11.services.DisbursementPreference;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;

public class DisbursementClientTest {

    private static final Logger LOG = Logger.getLogger(DisbursementClient.class.getName());
    DisbursementClient disbursementClient;
    CreditRequest cr;
    DisbursementPreference serviceDp;

    @Before
    public void setUp() throws PropertyException {
        disbursementClient = new DisbursementClient();
        cr = Data.createCreditRequest();
        Address a = new Address("1", "myCity", "2", "myHouse", "myStreet", "2170", "AUT");
        cr.getCustomer().setAddress(a);
        Cheque cheque = new Cheque();
        cheque.setName("Horstl");
        serviceDp = cheque;
    }

    @Test
    public void sourceGeneration() throws JAXBException, TransformerException {
        Source source = disbursementClient.getStartMoneyTransferProcessAsXml(cr, serviceDp);
        LOG.info(disbursementClient.sourceToString(source));
    }

    @Test
    public void disburseByBankTransfer() throws Exception {
        disbursementClient.disburse(cr);
    }

    @Test
    public void disburseByCheque() throws Exception {
        cr.getCustomer().setDisbursementPreference(new at.ac.tuwien.infosys.aic11.dto.Cheque("Ferdinand"));
        disbursementClient.disburse(cr);
    }
}
