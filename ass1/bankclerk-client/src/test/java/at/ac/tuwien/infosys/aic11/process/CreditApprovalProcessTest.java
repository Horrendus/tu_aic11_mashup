package at.ac.tuwien.infosys.aic11.process;

import at.ac.tuwien.infosys.aic11.dto.CreditRequest;
import at.ac.tuwien.infosys.aic11.dto.CreditRequestState;
import at.ac.tuwien.infosys.aic11.dto.Duration;
import at.ac.tuwien.infosys.aic11.main.Data;
import org.junit.Test;

import java.math.BigDecimal;

import static junit.framework.Assert.assertEquals;

public class CreditApprovalProcessTest {

    @Test
    public void testCalculation() {
        CreditApprovalProcess cap = new CreditApprovalProcess();

        assertEquals(new BigDecimal("9.75"), cap.calculateInterestRate(new BigDecimal("2.5"), new BigDecimal("5"), new Duration(2)));
    }
}
