package at.ac.tuwien.infosys.aic11.service;

import at.ac.tuwien.infosys.aic11.dto.Rating;
import at.ac.tuwien.infosys.aic11.main.MainUtils;
import at.ac.tuwien.infosys.aic11.services.CustomerRating;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.provider.JSONProvider;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;


/**
 * Created by IntelliJ IDEA.
 * User: hasel
 * Date: 11/11/11
 * Time: 5:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class RatingServiceTest {
    private static CustomerRating ratingService;

    @BeforeClass
    public static void setUp(){
        MainUtils.createREST_Service();
        JSONProvider jsonProvider = new JSONProvider();
        Map<String,String> inElements = new HashMap<String, String>();
        inElements.put("customer", "{http://at.ac.tuwien.infosys.aic11.dto}customer");
        inElements.put("bank_transfer","{http://at.ac.tuwien.infosys.aic11.dto}bank_transfer");
        inElements.put("cheque","{http://at.ac.tuwien.infosys.aic11.dto}cheque");
        inElements.put("credit_request", "{http://at.ac.tuwien.infosys.aic11.dto}credit_request");
        inElements.put("rating", "{http://at.ac.tuwien.infosys.aic11.dto}rating");

        jsonProvider.setInTransformElements(inElements);
        List<Object> providers = new ArrayList<Object>();
        providers.add(jsonProvider);
        ratingService = JAXRSClientFactory.create(MainUtils.REST_ADDRESS,CustomerRating.class,providers);
        System.out.printf("started RatingService");

    }

    @Test
    public void testGetCustomerRating(){
        Rating rating = ratingService.getRatingForCustomer("0");
        assertEquals("0",rating.getCustomer().getCustomerId());
    }

}
