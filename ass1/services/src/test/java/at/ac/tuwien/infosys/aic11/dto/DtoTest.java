package at.ac.tuwien.infosys.aic11.dto;

import at.ac.tuwien.infosys.aic11.main.Data;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;

import static junit.framework.Assert.assertEquals;

public class DtoTest {

    Marshaller marshaller;
    Unmarshaller unmarshaller;
    File tmpFile;
    CreditRequest creditRequest;

    @Before
    public void setUp() throws JAXBException, IOException {
        File tmpDir = new File("target/tmp");
        tmpDir.mkdirs();
        tmpDir.deleteOnExit();
        tmpFile = new File(tmpDir, "test.xml");
        tmpFile.createNewFile();
        tmpFile.deleteOnExit();
        JAXBContext jaxbContext = JAXBContext.newInstance(CreditRequest.class);
        unmarshaller = jaxbContext.createUnmarshaller();
        marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        creditRequest = Data.createCreditRequest();
    }

    @Test
    public void test() throws JAXBException {
//        marshaller.marshal(creditRequest, System.out);
        marshaller.marshal(creditRequest, tmpFile);

        CreditRequest c = (CreditRequest) unmarshaller.unmarshal(tmpFile);

        creditRequest.getCustomer().setRating(null);
        for (Warrantor w : creditRequest.getWarrantors()) {
            w.setRating(null);
        }
        assertEquals(creditRequest, c);
    }

    @Ignore
    @Test
    public void testRating() throws JAXBException {
        Rating r = new Rating(null, CustomerRating.A);
        marshaller.marshal(r, System.out);
    }
}
