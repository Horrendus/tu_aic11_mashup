package at.ac.tuwien.infosys.aic11.main;

import at.ac.tuwien.infosys.aic11.dto.Address;
import at.ac.tuwien.infosys.aic11.dto.BankTransfer;
import at.ac.tuwien.infosys.aic11.dto.CreditRequest;
import at.ac.tuwien.infosys.aic11.dto.CreditRequestState;
import at.ac.tuwien.infosys.aic11.dto.Customer;
import at.ac.tuwien.infosys.aic11.dto.CustomerRating;
import at.ac.tuwien.infosys.aic11.dto.Duration;
import at.ac.tuwien.infosys.aic11.dto.Money;
import at.ac.tuwien.infosys.aic11.dto.Offer;
import at.ac.tuwien.infosys.aic11.services.utils.CustomerNotFoundException;

import javax.xml.soap.SOAPException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;

public class LegacyApp {

    private static final Logger LOG = Logger.getLogger(LegacyApp.class.getName());

    private static final String CONTRACT_NOT_FOUND = "The requested contract is not managed by this service.";
    private static final String ILLEGAL_STATE_TRANSISTION = "Unable to set new state, invalid state progression requested.";

    private static final LegacyApp INSTANCE = new LegacyApp();

    private Map<String, Customer> customerMap = new HashMap<String,Customer>();
    private Map<String, CreditRequest> contracts = new HashMap<String, CreditRequest>();


    private LegacyApp(){
         initializeCustomers();
    }

    public static LegacyApp getInstance() {
        return INSTANCE;
    }


    private void initializeCustomers(){
        Customer customer;
        String strI;
        for(int i = 0; i < 10; i++){
            strI = String.valueOf(i);
            Address address = new Address("ad"+strI, "city" + strI, "door" + strI, "house" + strI, "street" + strI, "1090", "AUT");
           customer = new Customer(strI, "custFirst" + strI, "custLast"+ strI, new BigDecimal(100*i+10),
                new BankTransfer("Raika", "239003-AKJ"+strI, "0198098-AKJDN23_" +strI), address);
            customerMap.put(customer.getCustomerId(), customer);
        }
    }

    public Collection<Customer> getAllCustomers(){
        return this.customerMap.values();
    }


    public Customer getCustomerById(String customerId) {
        if(customerMap.containsKey(customerId)){
             return customerMap.get(customerId);
        }else{
            throw new CustomerNotFoundException();
        }
    }

    public CustomerRating getRandomCustomerRating(){
        int pick = new Random().nextInt(CustomerRating.values().length);
        return CustomerRating.values()[pick];
    }



    public CreditRequest createContract(String reason, Customer customer, Money money, Duration duration) {
        String contractId = java.util.UUID.randomUUID().toString();
        CreditRequest cr = new CreditRequest(contractId, reason, customer, money, duration);
        cr.setState(CreditRequestState.Offer);
        this.contracts.put(contractId, cr);
        return this.contracts.get(contractId);
    }

    public CreditRequest attachOffer(String contractId, Offer offer) throws Exception {
        try {
            CreditRequest c = this.contracts.get(contractId);
            c.setOffer(offer);
            return c;
        } catch (NullPointerException e) {
            throw new Exception(LegacyApp.CONTRACT_NOT_FOUND);
        }
    }

    public CreditRequest updateContract(String contractId, Money money, Duration duration) throws Exception {
        try {
            CreditRequest c = this.contracts.get(contractId);
            if (c.getState() == CreditRequestState.Offer) {
                c.setMoney(money);
                c.setDuration(duration);
                return c;
            } else {
                throw new Exception(LegacyApp.ILLEGAL_STATE_TRANSISTION);
            }
        } catch (NullPointerException e) {
            throw new Exception(LegacyApp.CONTRACT_NOT_FOUND);
        }
    }

    public void deleteContract(String contractId) throws Exception {
        if (this.contracts.remove(contractId) == null) {
            throw new Exception(LegacyApp.CONTRACT_NOT_FOUND);
        }
    }

    public void acceptContract(String contractId) throws Exception {
        this.advanceState(contractId, CreditRequestState.Offer, CreditRequestState.Accept);
    }

    public Collection<CreditRequest> getContracts(CreditRequestState state) {
        if (state == CreditRequestState.Undefined) {
            return this.contracts.values();
        }
        Collection<CreditRequest> l = new ArrayList<CreditRequest>();
        for (CreditRequest c : this.contracts.values()) {
            if (c.getState() == state) {
                l.add(c);
            }
        }
        return l;
    }

    public CreditRequest getContract(String contractId) throws Exception {
        CreditRequest cr = this.contracts.get(contractId);
        if (cr == null) {
            throw new Exception(LegacyApp.CONTRACT_NOT_FOUND);
        }
        return cr;
    }

    public void finalizeContract(String contractId) throws Exception {
        this.advanceState(contractId, CreditRequestState.AwaitingApproval, CreditRequestState.Final);
    }

    public void waitingForApproval(String contractId) throws Exception {
        this.advanceState(contractId, CreditRequestState.Accept, CreditRequestState.AwaitingApproval);
    }

    /**
     * Advance the contract state to <code>to</code> iif the current state is <code>from</code>.
     *
     * @param contractId the id of the contract to be advanced
     * @param from the starting state the contract must be in
     * @param to the target state that will be set if conditions are met
     * @throws SOAPException if the contract does not exist or is in the wrong state.
     */
    private void advanceState(String contractId, CreditRequestState from, CreditRequestState to) throws Exception {
        try {
            CreditRequest c = this.contracts.get(contractId);
            if (c.getState() == from) {
                c.setState(to);
            } else {
                throw new Exception(LegacyApp.ILLEGAL_STATE_TRANSISTION);
            }
        } catch (NullPointerException e) {
            throw new Exception(LegacyApp.CONTRACT_NOT_FOUND);
        }
    }

    public Boolean shipContract(String creditRequestId) {
        LOG.info("shipped Contract to customer! ");
        LOG.info("waiting for customer approval.");
        return true;
    }
}
