package at.ac.tuwien.infosys.aic11.services.utils;

import javax.ws.rs.WebApplicationException;

public class CustomerNotFoundException extends WebApplicationException {

  private static final long serialVersionUID = -7037443749675993854L;

    public CustomerNotFoundException( ) {
    }

}