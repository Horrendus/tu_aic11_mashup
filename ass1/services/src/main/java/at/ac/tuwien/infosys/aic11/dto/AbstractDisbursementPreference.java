package at.ac.tuwien.infosys.aic11.dto;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.adapters.XmlAdapter;

@XmlSeeAlso({Cheque.class, BankTransfer.class})
public abstract class AbstractDisbursementPreference implements DisbursementPreference {

    static class Adapter extends XmlAdapter<AbstractDisbursementPreference, DisbursementPreference> {
        public DisbursementPreference unmarshal(AbstractDisbursementPreference v) {
            return v;
        }

        public AbstractDisbursementPreference marshal(DisbursementPreference v) {
            return (AbstractDisbursementPreference) v;
        }
    }
}
