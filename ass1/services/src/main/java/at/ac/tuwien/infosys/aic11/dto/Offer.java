package at.ac.tuwien.infosys.aic11.dto;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlAccessorType(XmlAccessType.FIELD)
public class Offer {
    @XmlAttribute(required = true, name = "offer_id")
    private String offerId;

    @XmlElement(required = true)
    private String comments;

    @XmlElement(required = true, name = "interest_rate")
    private InterestRate interestRate;

    @XmlTransient
    private CreditRequest creditRequest;

    public Offer(String offerId, String comments, InterestRate interestRate, CreditRequest creditRequest) {
        this.offerId = offerId;
        this.comments = comments;
        this.interestRate = interestRate;
        this.creditRequest = creditRequest;
    }

    public Offer() {
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public InterestRate getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(InterestRate interestRate) {
        this.interestRate = interestRate;
    }

    public CreditRequest getCreditRequest() {
        return creditRequest;
    }

    public void setCreditRequest(CreditRequest creditRequest) {
        this.creditRequest = creditRequest;
    }

    public void afterUnmarshal(Unmarshaller u, Object parent) {
        creditRequest = (CreditRequest) parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Offer offer = (Offer) o;

        if (comments != null ? !comments.equals(offer.comments) : offer.comments != null) return false;
        if (creditRequest != null) {
            if (offer.creditRequest == null) {
                return false;
            }
            if (creditRequest.getRequestId() != null ? !creditRequest.getRequestId().equals(offer.creditRequest.getRequestId()) : offer.creditRequest.getRequestId() != null)
                return false;
        } else if (offer.creditRequest != null) {
            return false;
        }
        if (interestRate != null ? !interestRate.equals(offer.interestRate) : offer.interestRate != null) return false;
        if (offerId != null ? !offerId.equals(offer.offerId) : offer.offerId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = offerId != null ? offerId.hashCode() : 0;
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (interestRate != null ? interestRate.hashCode() : 0);
        if (creditRequest != null) {
            result = 31 * result + (creditRequest.getRequestId() != null ? creditRequest.getRequestId().hashCode() : 0);
        }
        return result;
    }
}
