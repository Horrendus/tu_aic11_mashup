package at.ac.tuwien.infosys.aic11.services.impl;

import at.ac.tuwien.infosys.aic11.dto.CreditRequest;
import at.ac.tuwien.infosys.aic11.dto.CreditRequestState;
import at.ac.tuwien.infosys.aic11.dto.Customer;
import at.ac.tuwien.infosys.aic11.dto.Duration;
import at.ac.tuwien.infosys.aic11.dto.Money;
import at.ac.tuwien.infosys.aic11.main.LegacyApp;
import at.ac.tuwien.infosys.aic11.services.ContractManagement;
import at.ac.tuwien.infosys.aic11.services.logging.LogUtils;

import javax.jws.WebParam;
import java.util.Collection;
import java.util.logging.Logger;

public class ContractManagementImpl implements ContractManagement {

    private static final Logger LOG = LogUtils.createLogger(ContractManagementImpl.class.getName());
    private LegacyApp legacyApp = LegacyApp.getInstance();

    @Override
    public CreditRequest createContract(String reason, Customer customer, Money money, Duration duration) {
        LOG.entering(ContractManagementImpl.class.getName(), "createContract", new Object[] { reason, customer, money, duration });
        if (reason == null) {
            LOG.severe("reason is null");
            throw new IllegalArgumentException("reason is null");
        }
        if (customer == null) {
            LOG.severe("customer is null");
            throw new IllegalArgumentException("customer is null");
        }
        if (money == null) {
            LOG.severe("money is null");
            throw new IllegalArgumentException("money is null");
        }
        if (duration == null) {
            LOG.severe("duration is null");
            throw new IllegalArgumentException("duration is null");
        }
        CreditRequest result = legacyApp.createContract(reason, customer, money, duration);
        LOG.entering(ContractManagementImpl.class.getName(), "createContract", result);
        return result;
    }

    @Override
    public CreditRequest attachOffer(CreditRequest creditRequest) {
        LOG.entering(ContractManagementImpl.class.getName(), "attachOffer", creditRequest);
        if (creditRequest == null) {
            LOG.severe("creditRequest is null");
            throw new IllegalArgumentException("creditRequest is null");
        }
        if (creditRequest.getRequestId() == null) {
            LOG.severe("creditRequestId is null");
            throw new IllegalArgumentException("creditRequestId is null");
        }
        if (creditRequest.getOffer() == null) {
            LOG.severe("offer is null");
            throw new IllegalArgumentException("offer is null");
        }
        try {
            CreditRequest result = legacyApp.attachOffer(creditRequest.getRequestId(), creditRequest.getOffer());
            LOG.exiting(ContractManagementImpl.class.getName(), "attachOffer", result);
            return result;
        } catch (Exception e) {
            LOG.severe("epic fail in attachOffer");
            LOG.fine(e.getStackTrace().toString());
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public CreditRequest updateContract(String contractId, Money money, Duration duration) {
        LOG.entering(ContractManagementImpl.class.getName(), "updateContract", new Object[] { contractId, money, duration });
        if (contractId == null) {
            LOG.severe("contractID is null");
            throw new IllegalArgumentException("contractId is null");
        }
        if (money == null) {
            LOG.severe("money is null");
            throw new IllegalArgumentException("money is null");
        }
        if (duration == null) {
            LOG.severe("duration is null");
            throw new IllegalArgumentException("duration is null");
        }
        try {
            CreditRequest result = legacyApp.updateContract(contractId, money, duration); 
            LOG.exiting(ContractManagementImpl.class.getName(), "updateContract", result);
            return result;
        } catch (Exception e) {
            LOG.severe("epic fail in updateContract");
            LOG.fine(e.getStackTrace().toString());
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteContract(String contractId) {
        LOG.entering(ContractManagementImpl.class.getName(), "deleteContract", contractId);
        if (contractId == null) {
            LOG.severe("contractId is null");
            throw new IllegalArgumentException("contractId is null");
        }
        try {
            LOG.exiting(ContractManagementImpl.class.getName(), "deleteContract");
            legacyApp.deleteContract(contractId);
        } catch (Exception e) {
            LOG.severe("epic fail in deleteContract");
            LOG.fine(e.getStackTrace().toString());
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void acceptContract(String contractId) {
        LOG.entering(ContractManagementImpl.class.getName(), "acceptContract", contractId);
        if (contractId == null) {
            LOG.severe("contractId is null");
            throw new IllegalArgumentException("contractId is null");
        }
        try {
            LOG.exiting(ContractManagementImpl.class.getName(), "acceptContract");
            legacyApp.acceptContract(contractId);
        } catch (Exception e) {
            LOG.severe("epic fail in acceptContract");
            LOG.fine(e.getStackTrace().toString());
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Collection<CreditRequest> getContracts(CreditRequestState state) {
        LOG.entering(ContractManagementImpl.class.getName(), "getContracts", state);
        LOG.exiting(ContractManagementImpl.class.getName(), "getContracts");
        return legacyApp.getContracts(state);
    }

    @Override
    public CreditRequest getContract(String contractId) {
        LOG.entering(ContractManagementImpl.class.getName(), "getContract", contractId);
        if (contractId == null) {
            LOG.severe("contractId is null");
            throw new IllegalArgumentException("contractId is null");
        }
        try {
            CreditRequest result = legacyApp.getContract(contractId); 
            LOG.exiting(ContractManagementImpl.class.getName(), "getContracts", result);
            return result;
        } catch (Exception e) {
            LOG.severe("epic fail in getContract");
            LOG.fine(e.getStackTrace().toString());
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void finalizeContract(String contractId) {
        LOG.entering(ContractManagementImpl.class.getName(), "finalizeContract", contractId);
        if (contractId == null) {
            LOG.severe("contractId is null");
            throw new IllegalArgumentException("contractId is null");
        }
        try {
            LOG.exiting(ContractManagementImpl.class.getName(), "finalizeContract");
            legacyApp.finalizeContract(contractId);
        } catch (Exception e) {
            LOG.severe("epic fail in finalizeContract");
            LOG.fine(e.getStackTrace().toString());
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void awaitingContractApproval(@WebParam(name = "contract_id") String contractId) {
        LOG.entering(ContractManagementImpl.class.getName(), "awaitingContractApproval", contractId);
        if (contractId == null) {
            LOG.severe("contractId is null");
            throw new IllegalArgumentException("contractId is null");
        }
        try {
            LOG.exiting(ContractManagementImpl.class.getName(), "awaitingContractApproval");
            legacyApp.waitingForApproval(contractId);
        } catch (Exception e) {
            LOG.severe("epic fail in awaitingContractApproval");
            LOG.fine(e.getStackTrace().toString());
            throw new IllegalArgumentException(e);
        }
    }

}
