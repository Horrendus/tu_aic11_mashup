package at.ac.tuwien.infosys.aic11.services;

import at.ac.tuwien.infosys.aic11.dto.Customer;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService(targetNamespace = "http://at.ac.tuwien.infosys.aic11.services")
public interface CustomerManagement {
  
    /**
     * retrieves a Customer object by it's id
     * @param customerId the id of the customer
     * @return the customer object or null
     */
    public Customer getCustomerById(@WebParam(name = "customer_id") String customerId);
    
    /**
     * @return all Customers
     */
    public Collection<Customer> getAll();
}
