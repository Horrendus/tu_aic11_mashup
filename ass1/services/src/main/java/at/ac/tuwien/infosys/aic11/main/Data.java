package at.ac.tuwien.infosys.aic11.main;

import at.ac.tuwien.infosys.aic11.dto.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Data {
    private Data() { }
    
    public static CreditRequest createCreditRequest() {
        AbstractDisbursementPreference da = new BankTransfer("Raika", "239003-AKJ", "0198098-AKJDN23");
        Customer customer = new Customer("1", "custFirst", "custLast", new BigDecimal(100), da
                , new Address("1"));
        CreditRequest creditRequest = new CreditRequest("10", "reason", customer, new Money("EUR", 200L), new Duration(2));
        customer.getCreditRequests().add(creditRequest);
        List<Warrantor> warrantors = new ArrayList<Warrantor>();
        Warrantor w = new Warrantor("2", "firstName", "lastName", new BigDecimal(300), new Cheque("Stefan Koubek"),
                new Address("2"));
        w.setRating(new Rating(w, CustomerRating.AAA));
        Warrantor w2 = new Warrantor("3", "Vladimir", "Vladimir", new BigDecimal(500), new Cheque("Vladimir"),
                new Address("4"));
        warrantors.add(w);
        warrantors.add(w2);
        creditRequest.setWarrantors(warrantors);
        Offer o = new Offer("3", "comments", new InterestRate(3.2), creditRequest);
        creditRequest.setOffer(o);
        return creditRequest;
    }

    public static String encodeToXml(CreditRequest creditRequest) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(CreditRequest.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            StringWriter writer = new StringWriter();
            marshaller.marshal(creditRequest, writer);
            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
