package at.ac.tuwien.infosys.aic11.main;

public class AllMain {
    public static void main(String[] args) {
        ShippingMain.startShippingService();
        RatingMain.startRatingService();
        CustomerManagementMain.startService();
        
        
        //RatingClientMain.startRatingClient();
        // must be started LAST or all the other services will be encrypted as well
        ContractManagementMain.startService();
        MainUtils.exitOnUserInput();
    }
}
