package at.ac.tuwien.infosys.aic11.main;

public class RatingMain {
    public static void main(String[] args) {
        startRatingService();
        MainUtils.exitOnUserInput();
    }

    public static void startRatingService() {
         MainUtils.createREST_Service();
    }
}
