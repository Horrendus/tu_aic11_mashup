package at.ac.tuwien.infosys.aic11.dto;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "bank_transfer")
@XmlType(propOrder = {
        "bankName",
        "bic",
        "iban"
})
public class BankTransfer extends AbstractDisbursementPreference {
    @XmlElement(name = "bank_name")
    private String bankName;

    @XmlElement(required = true)
    private String bic;

    @XmlElement(required = true)
    private String iban;

    public BankTransfer(String bankName, String bic, String iban) {
        this.bankName = bankName;
        this.bic = bic;
        this.iban = iban;
    }

    public BankTransfer(String bic, String iban) {
        this.bic = bic;
        this.iban = iban;
    }

    public BankTransfer() {
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BankTransfer that = (BankTransfer) o;

        if (bankName != null ? !bankName.equals(that.bankName) : that.bankName != null) return false;
        if (bic != null ? !bic.equals(that.bic) : that.bic != null) return false;
        if (iban != null ? !iban.equals(that.iban) : that.iban != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = bankName != null ? bankName.hashCode() : 0;
        result = 31 * result + (bic != null ? bic.hashCode() : 0);
        result = 31 * result + (iban != null ? iban.hashCode() : 0);
        return result;
    }
}
