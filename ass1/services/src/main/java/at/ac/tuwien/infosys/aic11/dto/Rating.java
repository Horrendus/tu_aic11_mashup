package at.ac.tuwien.infosys.aic11.dto;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "rating")
@XmlAccessorType(XmlAccessType.FIELD)
public class Rating {


    @XmlElement(required = true, name = "customer")
    private Customer customer;

    @XmlElement(required = true, name = "customer_rating")
    private CustomerRating customerRating;

    public Rating(Customer customer, CustomerRating customerRating) {
        this.customer = customer;
        this.customerRating = customerRating;
    }

    public Rating() {
    }

    public CustomerRating getCustomerRating() {
        return customerRating;
    }

    public void setCustomerRating(CustomerRating customerRating) {
        this.customerRating = customerRating;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rating rating = (Rating) o;

        if (customer != null) {
            if (rating.customer == null) {
                return false;
            }
            if (customer.getCustomerId() != null ? !customer.getCustomerId().equals(rating.customer.getCustomerId()) : rating.customer.getCustomerId() != null)
                return false;
        } else if (rating.customer != null) {
            return false;
        }
        if (customerRating != rating.customerRating) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (customerRating != null ? customerRating.hashCode() : 0);
        if (customer != null) {
            result = 31 * result + customer.getCustomerId() != null ? customer.getCustomerId().hashCode() : 0;
        }
        return result;
    }
}
