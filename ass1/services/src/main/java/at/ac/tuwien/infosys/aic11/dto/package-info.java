@XmlSchema(
    namespace = "http://at.ac.tuwien.infosys.aic11.dto",
    elementFormDefault = XmlNsForm.QUALIFIED)
package at.ac.tuwien.infosys.aic11.dto;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;