package at.ac.tuwien.infosys.aic11.main;

import java.util.logging.Level;
import java.util.logging.Logger;

import at.ac.tuwien.infosys.aic11.services.impl.ContractManagementImpl;

public class ContractManagementMain {
  
    private static Logger LOG =  Logger.getLogger(ContractManagementMain.class.getCanonicalName());
    
    public static void main(String[] args) {
        startService();
        MainUtils.exitOnUserInput();
    }

    public static void startService() {
        try {
          MainUtils.createEncryptedService(new ContractManagementImpl());
        } catch (InterruptedException e) {
          LOG.log(Level.SEVERE, "Could not start encrypted ContractMangementService.");
          LOG.log(Level.FINE, e.getStackTrace().toString());
        }
    }
}
