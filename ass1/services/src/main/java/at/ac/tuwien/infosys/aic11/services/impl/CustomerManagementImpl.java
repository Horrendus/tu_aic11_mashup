package at.ac.tuwien.infosys.aic11.services.impl;

import at.ac.tuwien.infosys.aic11.dto.Customer;
import at.ac.tuwien.infosys.aic11.main.LegacyApp;
import at.ac.tuwien.infosys.aic11.services.CustomerManagement;
import at.ac.tuwien.infosys.aic11.services.logging.LogUtils;

import java.util.Collection;
import java.util.logging.Logger;

public class CustomerManagementImpl implements CustomerManagement {

    private LegacyApp legacyApp = LegacyApp.getInstance();
    private static final Logger LOG = LogUtils.createLogger(CustomerManagementImpl.class.getName());

    @Override
    public Customer getCustomerById(String customerId) {
        LOG.entering(CustomerManagementImpl.class.getName(), "getCustomerById", customerId);
        if (customerId == null) {
            LOG.severe("customerId is null");
            throw new IllegalArgumentException("customerId == null");
        }
        Customer result = legacyApp.getCustomerById(customerId);
        LOG.exiting(CustomerManagementImpl.class.getName(), "getCustomerById", result);
        return result;
    }

    @Override
    public Collection<Customer> getAll() {
        LOG.entering(CustomerManagementImpl.class.getName(), "getAll");
        Collection<Customer> result = legacyApp.getAllCustomers();
        LOG.exiting(CustomerManagementImpl.class.getName(), "getAll", result);
        return result;
    }
}
