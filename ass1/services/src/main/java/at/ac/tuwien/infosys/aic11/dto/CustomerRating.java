package at.ac.tuwien.infosys.aic11.dto;

import java.math.BigDecimal;

public enum CustomerRating {
    AAA("0"),
    AAPlus("0.1"),
    AA("0.15"),
    AAMinus("0.2"),
    APlus("0.25"),
    A("0.3"),
    AMinus("0.35"),
    Defaulting("0.4");

    private BigDecimal value;

    CustomerRating(String value) {
        this.value = new BigDecimal(value);
    }

    public BigDecimal getValue() {
        return value;
    }
}
