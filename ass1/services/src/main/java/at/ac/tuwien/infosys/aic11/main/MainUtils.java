package at.ac.tuwien.infosys.aic11.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.provider.JSONProvider;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;

import at.ac.tuwien.infosys.aic11.services.impl.CustomerRatingImpl;
import at.ac.tuwien.infosys.aic11.services.logging.LogUtils;
import at.ac.tuwien.infosys.aic11.services.logging.LoggingInInterceptor;
import at.ac.tuwien.infosys.aic11.services.logging.LoggingOutInterceptor;
import at.ac.tuwien.infosys.aic11.services.utils.CustomerNotFoundMapper;


public class MainUtils {
    public static final String WS_ADDRESS = "http://localhost:9000/";
    public static final String REST_ADDRESS = "http://localhost:9010/resources";
    public static final String WSU_NS = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
    
    private static Logger LOG =  Logger.getLogger(MainUtils.class.getCanonicalName());

    private MainUtils() { }

    public static void exitOnUserInput() {
        LOG.info("Press any button to exit...");
        try {
            System.in.read();
        } catch (IOException e) {
          LOG.info("Closing service");
          System.exit(0);
        }
    }

    public static String getRelServicePath(Class<?> serviceType) {
        return serviceType.getSimpleName().toLowerCase();
    }

    public static Server createService(Object serviceBean) {
        String serviceImplName = serviceBean.getClass().getSimpleName();
        LOG.info(String.format("Starting %s...%n", serviceImplName));
        JaxWsServerFactoryBean svrFactory = new JaxWsServerFactoryBean();
        Class<?> serviceType = serviceBean.getClass().getInterfaces()[0];
        svrFactory.setServiceClass(serviceType);
        svrFactory.setAddress(String.format("%s%s", WS_ADDRESS, getRelServicePath(serviceType)));
        svrFactory.setServiceBean(serviceBean);
        Logger logger = LogUtils.createLogger(serviceImplName);
        svrFactory.getInInterceptors().add(new LoggingInInterceptor(logger));
        svrFactory.getOutInterceptors().add(new LoggingOutInterceptor(logger));
        LOG.info(String.format("%s created%n", serviceImplName));
        return svrFactory.create();
    }

    public static void createEncryptedService(Object serviceBean) throws InterruptedException {
        LOG.info("add encryption interceptors to " + getRelServicePath(serviceBean.getClass()));
        Map<String, Object> outProps = new HashMap<String, Object>();
        outProps.put("action", "UsernameToken Timestamp Signature Encrypt");
        outProps.put("passwordType", "PasswordText");
        outProps.put("user", "serverx509v1");
        outProps.put("passwordCallbackClass", "at.ac.tuwien.infosys.aic11.main.UTPasswordCallback");
        outProps.put("encryptionUser", "clientx509v1");
        outProps.put("encryptionPropFile", "etc/Server_SignVerf.properties");
        outProps.put("encryptionKeyIdentifier", "IssuerSerial");
        outProps.put("encryptionParts", "{Element}{" + WSU_NS + "}Timestamp;"
                + "{Content}{http://schemas.xmlsoap.org/soap/envelope/}Body");
        outProps.put("signaturePropFile", "etc/Server_Decrypt.properties");
        outProps.put("signatureKeyIdentifier", "DirectReference");
        outProps.put("signatureParts", "{Element}{" + WSU_NS + "}Timestamp;"
                + "{Element}{http://schemas.xmlsoap.org/soap/envelope/}Body");

        Map<String, Object> inProps = new HashMap<String, Object>();
        inProps.put("action", "UsernameToken Timestamp Signature Encrypt");
        inProps.put("passwordType", "PasswordDigest");
        inProps.put("passwordCallbackClass", "at.ac.tuwien.infosys.aic11.main.UTPasswordCallback");

        inProps.put("decryptionPropFile", "etc/Server_Decrypt.properties");
        inProps.put("encryptionKeyIdentifier", "IssuerSerial");

        inProps.put("signaturePropFile", "etc/Server_SignVerf.properties");
        inProps.put("signatureKeyIdentifier", "DirectReference");

        Server server = createService(serviceBean);
        Endpoint cxfEndpoint = server.getEndpoint();

        WSS4JInInterceptor wssIn = new WSS4JInInterceptor(inProps);
        cxfEndpoint.getInInterceptors().add(wssIn);

        WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
        cxfEndpoint.getOutInterceptors().add(wssOut);

        LOG.info("Encrypted Server ready...");
    }

    public static void createREST_Service() {
        String serviceImplName = CustomerRatingImpl.class.getSimpleName();
        LOG.info(String.format("Starting %s...%n", serviceImplName));

        JAXRSServerFactoryBean serverFactory = new JAXRSServerFactoryBean();
        serverFactory.setAddress(REST_ADDRESS);
        serverFactory.setResourceClasses(CustomerRatingImpl.class);

        JSONProvider jsonProvider = new JSONProvider();
        jsonProvider.setIgnoreNamespaces(true);

        List<Object> providerList = new ArrayList<Object>();
        providerList.add(new CustomerNotFoundMapper());
        providerList.add(jsonProvider);

        serverFactory.setProviders(providerList);

        Logger logger = LogUtils.createLogger(CustomerRatingImpl.class.getSimpleName());
        serverFactory.getInInterceptors().add(new LoggingInInterceptor(logger));
        serverFactory.getOutInterceptors().add(new LoggingOutInterceptor(logger));
        serverFactory.create();
        LOG.info(String.format("%s created%n", CustomerRatingImpl.class.getSimpleName()));
    }
}
