package at.ac.tuwien.infosys.aic11.services.impl;

import java.util.logging.Logger;

import at.ac.tuwien.infosys.aic11.main.LegacyApp;
import at.ac.tuwien.infosys.aic11.services.Shipping;
import at.ac.tuwien.infosys.aic11.services.logging.LogUtils;

import javax.jws.WebService;

@WebService
public class ShippingImpl implements Shipping {

    private LegacyApp legacyApp = LegacyApp.getInstance();
    private static final Logger LOG = LogUtils.createLogger(ShippingImpl.class.getName());

    @Override
    public Boolean shipContract(String creditRequestId) {
        LOG.entering(ShippingImpl.class.getName(), "shipContract", creditRequestId);
        Boolean result = legacyApp.shipContract(creditRequestId);
        LOG.exiting(ShippingImpl.class.getName(), "shipContract", result);
        return result;
    }
}
