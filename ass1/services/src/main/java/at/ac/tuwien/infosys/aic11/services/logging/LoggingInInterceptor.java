package at.ac.tuwien.infosys.aic11.services.logging;

import java.util.logging.Logger;

public class LoggingInInterceptor extends org.apache.cxf.interceptor.LoggingInInterceptor {

    private Logger logger;

    public LoggingInInterceptor(Logger logger) {
        this.logger = logger;
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected void log(String message) {
        super.log(LogUtils.format(logger, message));
    }
}
