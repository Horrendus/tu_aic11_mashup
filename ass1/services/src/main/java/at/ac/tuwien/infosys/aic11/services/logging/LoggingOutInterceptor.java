package at.ac.tuwien.infosys.aic11.services.logging;

import java.util.logging.Logger;

public class LoggingOutInterceptor extends org.apache.cxf.interceptor.LoggingOutInterceptor {

    private Logger logger;

    public LoggingOutInterceptor(Logger logger) {
        this.logger = logger;
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected void log(String message) {
        super.log(LogUtils.format(logger, message));
    }
}
