package at.ac.tuwien.infosys.aic11.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Duration {
    @XmlElement(required = true)
    private int years;

    public Duration(int years) {
        this.years = years;
    }

    public Duration() {
    }

    public int getYears() {
        return years;
    }

    public void setYears(int years) {
        this.years = years;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Duration duration = (Duration) o;

        if (years != duration.years) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return years;
    }
}
