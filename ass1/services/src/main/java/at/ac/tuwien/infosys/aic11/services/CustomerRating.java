package at.ac.tuwien.infosys.aic11.services;

import at.ac.tuwien.infosys.aic11.dto.Rating;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/customers")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface CustomerRating {

 @GET
 @Path("/{customerId}/rating")
 public Rating getRatingForCustomer(@PathParam("customerId") String customerId);
}
