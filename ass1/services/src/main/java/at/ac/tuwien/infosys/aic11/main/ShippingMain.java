package at.ac.tuwien.infosys.aic11.main;

import at.ac.tuwien.infosys.aic11.services.impl.ShippingImpl;

public class ShippingMain {
    public static void main(String[] args) {
        startShippingService();
        MainUtils.exitOnUserInput();
    }

    public static void startShippingService() {
         MainUtils.createService(new ShippingImpl());
    }
}
