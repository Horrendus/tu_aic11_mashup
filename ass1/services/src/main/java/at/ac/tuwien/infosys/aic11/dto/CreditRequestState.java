package at.ac.tuwien.infosys.aic11.dto;

public enum CreditRequestState {
  Undefined,
  Create,
  Offer,
  Update,
  Accept,
  AwaitingApproval,
  Delete,
  Final
}
