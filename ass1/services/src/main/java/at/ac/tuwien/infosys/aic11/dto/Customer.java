package at.ac.tuwien.infosys.aic11.dto;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "customer")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({Rating.class})
public class Customer {

    @XmlID
    @XmlAttribute(required = true, name = "customer_id")
    private String customerId;

    @XmlElement(required = true, name = "first_name")
    private String firstName;

    @XmlElement(name = "middle_name")
    private String middleName;

    @XmlElement(required = true, name = "last_name")
    private String lastName;

    @XmlElement(required = true, name = "open_balance")
    private BigDecimal openBalance;

    @XmlElement(required = true, name = "disbursement_preference")
    private DisbursementPreference disbursementPreference;

    @XmlElement(required = true)
    private Address address;

    @XmlTransient
    private Rating rating;

    @XmlIDREF
    @XmlElement(name = "credit_request")
    private List<CreditRequest> creditRequests = new ArrayList<CreditRequest>();

    public Customer(String customerId, String firstName, String lastName, BigDecimal openBalance,
                    DisbursementPreference disbursementPreference, Address address) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.openBalance = openBalance;
        this.disbursementPreference = disbursementPreference;
        this.address = address;
    }

    public Customer() {
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BigDecimal getOpenBalance() {
        return openBalance;
    }

    public void setOpenBalance(BigDecimal openBalance) {
        this.openBalance = openBalance;
    }

    public DisbursementPreference getDisbursementPreference() {
        return disbursementPreference;
    }

    public void setDisbursementPreference(DisbursementPreference disbursementPreference) {
        this.disbursementPreference = disbursementPreference;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public List<CreditRequest> getCreditRequests() {
        return creditRequests;
    }

    public void setCreditRequests(List<CreditRequest> creditRequests) {
        this.creditRequests = creditRequests;
    }

    @XmlTransient
    public String getName() {
        return firstName + " " + lastName;
    }

    @Override
    public String toString() {
        return customerId + " " + firstName + " " + lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (address != null ? !address.equals(customer.address) : customer.address != null) return false;
        if (customerId != null ? !customerId.equals(customer.customerId) : customer.customerId != null) return false;
        if (disbursementPreference != null ? !disbursementPreference.equals(customer.disbursementPreference) : customer.disbursementPreference != null)
            return false;
        if (firstName != null ? !firstName.equals(customer.firstName) : customer.firstName != null) return false;
        if (lastName != null ? !lastName.equals(customer.lastName) : customer.lastName != null) return false;
        if (middleName != null ? !middleName.equals(customer.middleName) : customer.middleName != null) return false;
        if (openBalance != null ? !openBalance.equals(customer.openBalance) : customer.openBalance != null)
            return false;
        if (rating != null ? !rating.equals(customer.rating) : customer.rating != null) return false;
        if (creditRequests != null) {
            if (customer.creditRequests == null || customer.creditRequests.size() != creditRequests.size()) {
                return false;
            }
            for (CreditRequest c : creditRequests) {
                boolean e = false;
                for (CreditRequest cthat : customer.creditRequests) {
                    if (c.getRequestId() != null ? c.getRequestId().equals(cthat.getRequestId()) : cthat.getRequestId() == null) {
                        e = true;
                        break;
                    }
                }
                if (e == false) {
                    return false;
                }
            }
        } else if (customer.creditRequests != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = customerId != null ? customerId.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (openBalance != null ? openBalance.hashCode() : 0);
        result = 31 * result + (disbursementPreference != null ? disbursementPreference.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        if (creditRequests != null) {
            for (CreditRequest c : creditRequests) {
                result = 31 * result + (c.getRequestId() != null ? c.getRequestId().hashCode() : 0);
            }
        }
        return result;
    }
}
