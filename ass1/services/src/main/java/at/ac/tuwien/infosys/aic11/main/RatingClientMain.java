package at.ac.tuwien.infosys.aic11.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.provider.JSONProvider;

import at.ac.tuwien.infosys.aic11.services.CustomerRating;

public class RatingClientMain {

    public static void main(String[] args) {
        startRatingClient();
        MainUtils.exitOnUserInput();
    }
    public static void startRatingClient(){

        JSONProvider jsonProvider = new JSONProvider();
        Map<String,String> inElements = new HashMap<String, String>();
        inElements.put("customer", "{http://at.ac.tuwien.infosys.aic11.dto}customer");
        inElements.put("bank_transfer","{http://at.ac.tuwien.infosys.aic11.dto}bank_transfer");
        inElements.put("cheque","{http://at.ac.tuwien.infosys.aic11.dto}cheque");
        inElements.put("credit_request", "{http://at.ac.tuwien.infosys.aic11.dto}credit_request");
        inElements.put("rating", "{http://at.ac.tuwien.infosys.aic11.dto}rating");


        jsonProvider.setInTransformElements(inElements);
        List<Object> providers = new ArrayList<Object>();
        providers.add(jsonProvider);

        CustomerRating customerRating = JAXRSClientFactory.create(MainUtils.REST_ADDRESS, CustomerRating.class, providers, true);
        customerRating.getRatingForCustomer("0");
    }
}
