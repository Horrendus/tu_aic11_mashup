package at.ac.tuwien.infosys.aic11.dto;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "credit_request")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditRequest {
    @XmlID
    @XmlAttribute(required = true, name = "request_id")
    private String requestId;

    @XmlElement(required = true)
    private String reason;

    @XmlElement(required = true)
    private Money money;

    @XmlElement(required = true)
    private Duration duration;

    private Offer offer;

    @XmlElement(name = "warrantor")
    private List<Warrantor> warrantors = new ArrayList<Warrantor>();

    @XmlElement(required = true)
    private Customer customer;
    
    @XmlElement(required = true)
    private CreditRequestState state;
    
    public CreditRequest() {}

    public CreditRequest(String requestId, String reason, Customer customer, Money money, Duration duration) {
        this.requestId = requestId;
        this.customer = customer;
        this.reason = reason;
        this.money = money;
        this.duration = duration;
        this.state = CreditRequestState.Offer;
    }
    
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Money getMoney() {
        return money;
    }

    public void setMoney(Money money) {
        this.money = money;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public List<Warrantor> getWarrantors() {
        return warrantors;
    }

    public void setWarrantors(List<Warrantor> warrantors) {
        this.warrantors = warrantors;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public CreditRequestState getState() {
      return state;
    }

    public void setState(CreditRequestState state) {
      this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CreditRequest that = (CreditRequest) o;

        if (customer != null ? !customer.equals(that.customer) : that.customer != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (money != null ? !money.equals(that.money) : that.money != null) return false;
        if (offer != null ? !offer.equals(that.offer) : that.offer != null) return false;
        if (reason != null ? !reason.equals(that.reason) : that.reason != null) return false;
        if (requestId != null ? !requestId.equals(that.requestId) : that.requestId != null) return false;
        if (warrantors != null ? !warrantors.equals(that.warrantors) : that.warrantors != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = requestId != null ? requestId.hashCode() : 0;
        result = 31 * result + (reason != null ? reason.hashCode() : 0);
        result = 31 * result + (money != null ? money.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (offer != null ? offer.hashCode() : 0);
        result = 31 * result + (warrantors != null ? warrantors.hashCode() : 0);
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        return result;
    }
}
