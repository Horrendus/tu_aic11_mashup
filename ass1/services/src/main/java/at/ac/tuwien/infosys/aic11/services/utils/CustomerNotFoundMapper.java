package at.ac.tuwien.infosys.aic11.services.utils;


import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class CustomerNotFoundMapper implements ExceptionMapper<CustomerNotFoundException> {

    public Response toResponse(CustomerNotFoundException exception) {
        return Response.status(Response.Status.NOT_FOUND).entity("{errorMessage:Customer does not exist}").type(MediaType.APPLICATION_JSON_TYPE).build();
    }

}

