package at.ac.tuwien.infosys.aic11.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
public class Warrantor extends Customer {
    @XmlIDREF
    @XmlElement(name = "warranted_credit_request")
    private List<CreditRequest> warrantedCreditRequests = new ArrayList<CreditRequest>();

    public Warrantor(String customerId, String firstName, String lastName, BigDecimal openBalance,
                     DisbursementPreference disbursementPreference, Address address) {
        super(customerId, firstName, lastName, openBalance, disbursementPreference, address);
    }

    public Warrantor(List<CreditRequest> warrantedCreditRequests) {
        this.warrantedCreditRequests = warrantedCreditRequests;
    }

    public Warrantor() {
    }

    public List<CreditRequest> getWarrantedCreditRequests() {
        return warrantedCreditRequests;
    }

    public void setWarrantedCreditRequests(List<CreditRequest> warrantedCreditRequests) {
        this.warrantedCreditRequests = warrantedCreditRequests;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Warrantor warrantor = (Warrantor) o;

        if (warrantedCreditRequests != null) {
            if (warrantor.warrantedCreditRequests == null || warrantor.warrantedCreditRequests.size() != warrantedCreditRequests.size()) {
                return false;
            }
            for (CreditRequest c : warrantedCreditRequests) {
                boolean e = false;
                for (CreditRequest cthat : warrantor.warrantedCreditRequests) {
                    if (c.getRequestId() != null ? c.getRequestId().equals(cthat.getRequestId()) : cthat.getRequestId() == null) {
                        e = true;
                        break;
                    }
                }
                if (e == false) {
                    return false;
                }
            }
        } else if (warrantor.warrantedCreditRequests != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        if (warrantedCreditRequests != null) {
            for (CreditRequest c : warrantedCreditRequests) {
                result = 31 * result + (c.getRequestId() != null ? c.getRequestId().hashCode() : 0);
            }
        }
        return result;
    }
}
