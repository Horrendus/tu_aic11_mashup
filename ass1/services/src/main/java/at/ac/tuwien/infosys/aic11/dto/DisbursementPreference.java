package at.ac.tuwien.infosys.aic11.dto;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlJavaTypeAdapter(AbstractDisbursementPreference.Adapter.class)
public interface DisbursementPreference {

}
