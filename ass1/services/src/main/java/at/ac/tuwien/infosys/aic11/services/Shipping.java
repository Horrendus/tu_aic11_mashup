package at.ac.tuwien.infosys.aic11.services;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(targetNamespace = "http://at.ac.tuwien.infosys.aic11.services")
public interface Shipping {

    /**
     * ships the contract to the customer
     * @param creditRequest the id of the contract for which the shipping order is given
     * @return success value
     */
    public Boolean shipContract(@WebParam(name = "creditRequest") String creditRequest);
}
