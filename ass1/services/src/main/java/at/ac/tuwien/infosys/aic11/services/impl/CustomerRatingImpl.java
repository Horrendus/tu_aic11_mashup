package at.ac.tuwien.infosys.aic11.services.impl;

import java.util.logging.Logger;

import at.ac.tuwien.infosys.aic11.dto.Customer;
import at.ac.tuwien.infosys.aic11.dto.Rating;
import at.ac.tuwien.infosys.aic11.main.LegacyApp;
import at.ac.tuwien.infosys.aic11.services.CustomerRating;
import at.ac.tuwien.infosys.aic11.services.logging.LogUtils;


public class CustomerRatingImpl implements CustomerRating {

    private LegacyApp legacyApp = LegacyApp.getInstance();
    private static final Logger LOG = LogUtils.createLogger(CustomerRatingImpl.class.getName());

    @Override
    public Rating getRatingForCustomer(String customerId) {
        LOG.entering(CustomerRatingImpl.class.getName(), "getRatingForCustomer", customerId);
        Customer customer = legacyApp.getCustomerById(customerId);
        Rating result = new Rating(customer,legacyApp.getRandomCustomerRating());
        LOG.exiting(CustomerRatingImpl.class.getName(), "getRatingForCustomer", result);
        return result;
    }
}
