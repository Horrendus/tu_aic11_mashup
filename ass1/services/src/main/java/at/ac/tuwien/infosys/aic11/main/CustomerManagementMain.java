package at.ac.tuwien.infosys.aic11.main;

import at.ac.tuwien.infosys.aic11.services.impl.CustomerManagementImpl;

public class CustomerManagementMain {
    public static void main(String[] args) {
        startService();
        MainUtils.exitOnUserInput();
    }

    public static void startService() {
        MainUtils.createService(new CustomerManagementImpl());
    }
}
