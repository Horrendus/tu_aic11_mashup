package at.ac.tuwien.infosys.aic11.services.logging;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LogUtils {
    private static Logger PARENT_LOGGER = Logger.getLogger("at.ac.tuwien.infosys.aic11.services");

    private LogUtils() { }

    public static Logger createLogger(String loggerName) {
        Logger logger = Logger.getLogger(loggerName);
        logger.setParent(PARENT_LOGGER);
        logger.getParent().setLevel(Level.SEVERE);
        logger.setLevel(Level.INFO);
        return logger;
    }

    public static String format(Logger logger, String message) {
        return String.format("%s %s", logger.getName(), message);
    }
}
