package at.ac.tuwien.infosys.aic11.services;

import at.ac.tuwien.infosys.aic11.dto.CreditRequest;
import at.ac.tuwien.infosys.aic11.dto.CreditRequestState;
import at.ac.tuwien.infosys.aic11.dto.Customer;
import at.ac.tuwien.infosys.aic11.dto.Duration;
import at.ac.tuwien.infosys.aic11.dto.Money;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService(targetNamespace = "http://at.ac.tuwien.infosys.aic11.services")
public interface ContractManagement {
  
  /**
   * Creates a new contract / credit request with the given data.
   * A new contract is initialized to the state "Offer".
   * 
   * @param reason String that describes the reason for the request
   * @param customer data structure that describes the customer information
   * @param money amount of money that shall be created
   * @param duration the intended duration of the credit
   * @return the newly created CreditRequest object
   */
  CreditRequest createContract(@WebParam(name = "reason") String reason,
      @WebParam(name = "customer") Customer customer, @WebParam(name = "money") Money money,
      @WebParam(name = "duration") Duration duration);

   /**
   * Attachs the offer attached to given credit request to the credit request of the background data.
   * The status of the contract remains "Offer".
   *
   * @param creditRequest
   * @return the newly created CreditRequest object
   */
  CreditRequest attachOffer(@WebParam(name = "credit_request") CreditRequest creditRequest);

  /**
   * Updates the contract by resetting the money and duration attributes.
   * The status of the contract remains "Offer".
   * 
   * @param contractId the id of the contract that shall be updated
   * @param money new amount of money to create
   * @param duration new duration of the credit
   * @return the updated CreditRequest object
   * @throws IllegalArgumentException in case the contract does not exist or is in the wrong state to be updated
   */
  CreditRequest updateContract(@WebParam(name = "contract_id") String contractId,
      @WebParam(name = "money") Money money, @WebParam(name = "duration") Duration duration);

  /**
   * Removes the contract. This happens when the customer does not accept the offer.
   * 
   * @param contractId id of the contract to be removed
   * @throws IllegalArgumentException in case the contract does not exist
   */
  void deleteContract(@WebParam(name = "contract_id") String contractId);
  
  /**
   * Advances the processing state of the contract to "Accept".
   * This happens when the customer accepts the credit offer.
   * 
   * @param contractId the id of the contract that was accepted
   * @throws IllegalArgumentException in case the contract does not exist or is in the wrong state to be accepted
   */
  void acceptContract(@WebParam(name = "contract_id") String contractId);
  
  /**
   * Advances the processing state of the contract to "Final".
   * This happens when the clerk has received the customers' fax.
   * 
   * @param contractId the id of the contract to finalize
   * @throws IllegalArgumentException in case the contract does not exist or is in the wrong state to be finalised
   */
  void finalizeContract(@WebParam(name = "contract_id") String contractId);

   /**
   * Advances the processing state of the contract to "AwaitingApproval".
   * This happens when the clerk has received the customers' fax.
   *
   * @param contractId the id of the contract to finalize
   * @throws IllegalArgumentException in case the contract does not exist or is in the wrong state to be finalised
   */
  void awaitingContractApproval(@WebParam(name = "contract_id") String contractId);

  
  /**
   * @param state the state that the returned contracts should be in
   * @return a list of all contracts with the given state
   */
  Collection<CreditRequest> getContracts(@WebParam(name = "state") CreditRequestState state);

    /**
   * @return the contract with the given id
   */
  CreditRequest getContract(@WebParam(name = "contract_id") String contractId);
}
