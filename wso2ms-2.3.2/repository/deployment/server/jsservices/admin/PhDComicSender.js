/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
this.serviceName = "PhDComicSender";
this.documentation = "This Service retrieves the latest PhD Comic and sends a link to it via Jabber. It shows how to read RSS Feeds & how to IM messages." ;

downloadAndSendComic.inputTypes = {"jabberid" : "string" };
function downloadAndSendComic(jabberid) {
    var url = retrieveLatestComic();
    sendComic(jabberid,url);
}

function retrieveLatestComic() {
    var feedReader = new FeedReader();
    var phdFeed = feedReader.get("http://www.phdcomics.com/gradfeed_justcomics.php")
    var feedEntries = phdFeed.getEntries();
    var latest = feedEntries[0].description;
    return latest;
}

sendComic.inputTypes = {"jabberid":"string","url":"string"};
function sendComic(jabberid,url) {
    var im = new IM("jabber");  
    im.login("horusbot@jabber.at","c0uch5urf!ng");
    im.sendMessage(jabberid,"Check out the following PhD Comic: " + url);
    im.disconnect();
}