/*
 * (2012)
 * JobsToday by Kevin Bader is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 */

this.serviceName = 'JobsToday';
this.documentation = 'A mashup which aggregates jobs from various websites';

getTodaysJobs.documentation = 'retrieves the jobs of today';
getTodaysJobs.inputTypes = { /* TODO: Add input types of this operation */ };
getTodaysJobs.outputType = 'XML'; /* TODO: Add output type here */ 
function getTodaysJobs()
{
	var jobs = <jobs></jobs>;
	
	jobs.appendChild(getTodaysJobsFromDerstandard());
	jobs.appendChild(<jobs />);
	
	return jobs;
}

getTodaysJobsFromDerstandard.documentation = 'retrieves todays jobs from derstandard.at';
getTodaysJobsFromDerstandard.inputTypes = { };
getTodaysJobsFromDerstandard.outputType = 'XML';
function getTodaysJobsFromDerstandard()
{
	var baseUrl = 'http://derstandard.at/anzeiger/derjob';
	var scrapeUrl = baseUrl + '/ResultList.aspx';
	
	var config =
		<config>
			<var-def name='response'>
				<html-to-xml>
					<http method='get' url={scrapeUrl}>
						<http-param name='Options'>50%2c49%2c48%2c47</http-param>
						<http-param name='decode'>1</http-param>
					</http>
				</html-to-xml>
			</var-def>
		</config>;
	
	var scraper = new Scraper(config);
	var result = scraper.response;
	
	// strip off the XML declaration and parse as XML.  
	var resultXML = new XML(result.substring(result.indexOf('?>') + 2));
	
	var jobsTableXML = resultXML
		['body']
		['div'].(@id == 'documentCanvas')
		['form']
		['table'][1]
		['tbody']
		['tr']
		['td']
		['div']
		['div']
		['table'][1]
		['tbody'];
	
	var jobs = <jobs sourceurl='http://derstandard.at/Karriere' sourcename='derstandard.at'></jobs>;
	
	var i = 0;
	for each (var row in jobsTableXML.children()) {
		jobs.appendChild(<job />);
		
		var job = jobs["job"][i];
		i++;
		
		job.appendChild(<link>{baseUrl}/{row['td'][1]['a']['@href'].toString()}</link>);
		job.appendChild(<title>{row['td'][1]['a'].toString()}</title>);
		job.appendChild(<company>{row['td'][2].toString()}</company>);
		job.appendChild(<location>{row['td'][3].toString()}</location>);
	}
	
	return jobs;
}

getTodaysJobsFromKurier.documentation = 'retrieves todays jobs from kurier.at';
getTodaysJobsFromKurier.inputTypes = { };
getTodaysJobsFromKurier.outputType = 'XML';
function getTodaysJobsFromKurier()
{
	var baseUrl = 'http://karrieren.kurier.at';
	var scrapeUrl = baseUrl + '/search.php';
	
	var config =
		<config>
			<var-def name='response'>
				<html-to-xml>
					<http method='post' url={scrapeUrl}>
						<http-param name='detailsearch.x'>26</http-param>
						<http-param name='detailsearch.y'>15</http-param>
						<http-param name='jobsearch[artid][]'>1</http-param>
						<http-param name='jobsearch[artid][]'>2</http-param>
						<http-param name='jobsearch[artid][]'>5</http-param>
						<http-param name='jobsearch[bezeichnung]'></http-param>
						<http-param name='jobsearch[tage]'>1</http-param>
						<http-param name='search'>init</http-param>
						<http-param name='searchSource'>ds</http-param>
					</http>
					<http method='get' url={scrapeUrl}>
						<http-param name='showerg'>1</http-param>
					</http>
				</html-to-xml>
			</var-def>
		</config>;
	
	var scraper = new Scraper(config);
	var result = scraper.response;
	
	// strip off the XML declaration and parse as XML.  
	var resultXML = new XML(result.substring(result.indexOf('?>') + 2));
	
	var jobsTableXML = resultXML
		['body']
		['div'].(@id == 'main')
		['div'].(@['class'] == 'spalte1')
		['div'].(@id == 'content');
		//['div'].(@['class'] == 'karrierecontents');
		//['table'].(@['class'] == 'karrieretreffer');
		//['tbody']
		//['tr'].(@['class'] == 'trefferaktuell');
	
	return jobsTableXML;
	
	var jobs = <jobs sourceurl='http://derstandard.at/Karriere' sourcename='derstandard.at'></jobs>;
	
	var i = 0;
	for each (var row in jobsTableXML.children()) {
		jobs.appendChild(<job />);
		
		var job = jobs["job"][i];
		i++;
		
		job.appendChild(<link>{baseUrl}/{row['td'][1]['a']['@href'].toString()}</link>);
		job.appendChild(<title>{row['td'][1]['a'].toString()}</title>);
		job.appendChild(<company>{row['td'][2].toString()}</company>);
		job.appendChild(<location>{row['td'][3].toString()}</location>);
	}
	
	return jobs;
}

                        

print_r.visible = false;
function print_r(theObj){
	var output;
	if (theObj.constructor == Array || theObj.constructor == Object) {
		output += "<ul>";
		for (var p in theObj) {
			if(theObj[p].constructor == Array || theObj[p].constructor == Object) {
				output += "<li>["+p+"] => "+typeof(theObj)+"</li>";
				output += "<ul>";
				output += print_r(theObj[p]);
				output += "</ul>";
			} else {
				output += "<li>["+p+"] => "+theObj[p]+"</li>";
			}
		}
		output += "</ul>";
	}
	return output;
}


                        