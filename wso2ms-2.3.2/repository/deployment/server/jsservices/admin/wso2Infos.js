/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
this.serviceName = "wso2Infos";
this.documentation = "Access REST Twitter & Facebook API to display Infos about wso2" ;

//getInfos.outputType = "XML";
function getInfos() {
    twitter = getTweets();
    facebook = getFBPosts();
    result = twitter + facebook;
    return result;
}

function getTweets() {
    var client = new HttpClient();
    var code = client.executeMethod("GET", "http://search.twitter.com/search.json?q=wso2");
    return client.response;
}
                        
function getFBPosts() {
    var client = new HttpClient();
    var code = client.executeMethod("GET", "https://graph.facebook.com/search?q=wso2&type=post");
    return client.response;
}